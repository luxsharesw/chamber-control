﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">Debug,False;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Libraries" Type="Folder">
			<Item Name="Plugin" Type="Folder">
				<Item Name="Chamber Control.lvlibp" Type="LVLibp" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp">
					<Item Name="Chamber Control Create.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/Chamber Control Create/Chamber Control Create.lvclass"/>
					<Item Name="Chamber Control.lvclass" Type="LVClass" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/Chamber Control/Chamber Control.lvclass"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get File Extension.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
					<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
					<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
					<Item Name="MGI String Pattern Match Exists.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/1abvi3w/user.lib/_MGI/String/MGI String Pattern Match Exists.vi"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Recursive File List.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/Chamber Control.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Support Library" Type="Folder">
			<Item Name="Type Sensitive Popup.lvlib" Type="Library" URL="../../../../Libraries/Type Sensitive Popup/Type Sensitive Popup.lvlib"/>
		</Item>
		<Item Name="Chamber Function.lvlib" Type="Library" URL="../Chamber Function/Chamber Function.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
				<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
				<Item Name="MGI Current VI&apos;s Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Current VI&apos;s Reference.vi"/>
				<Item Name="MGI Defer Panel Updates.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Defer Panel Updates.vi"/>
				<Item Name="MGI Level&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Level&apos;s VI Reference.vi"/>
				<Item Name="MGI Top Level VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Top Level VI Reference.vi"/>
				<Item Name="MGI VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference.vi"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="Set Cluster Element by Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Cluster Element by Name__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name=" Write Waveform to File (Digital).vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/ Write Waveform to File (Digital).vi"/>
				<Item Name=" Write Waveform to File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/ Write Waveform to File.vi"/>
				<Item Name=" Write Waveforms to File (1D).vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/ Write Waveforms to File (1D).vi"/>
				<Item Name=" Write Waveforms to File (2D).vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/ Write Waveforms to File (2D).vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="_ExponentialCalculation__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_ExponentialCalculation__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(I32)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(I32)__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(String)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(String)__lava_lib_ui_tools.vi"/>
				<Item Name="_fadetype__lava_lib_ui_tools.ctl" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_fadetype__lava_lib_ui_tools.ctl"/>
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="CenterRectInBnds.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/CenterRectInBnds.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close DWDT Array Dlog File+.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Close DWDT Array Dlog File+.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Close WDT Array Dlog File+.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Close WDT Array Dlog File+.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="compatReturnToEnter.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReturnToEnter.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="Delacor_lib_QMH_Cloneable Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Cloneable Module Admin_class/Delacor_lib_QMH_Cloneable Module Admin.lvclass"/>
				<Item Name="Delacor_lib_QMH_Message Queue.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Message Queue_class/Delacor_lib_QMH_Message Queue.lvclass"/>
				<Item Name="Delacor_lib_QMH_Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Module Admin_class/Delacor_lib_QMH_Module Admin.lvclass"/>
				<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Fade (Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade (Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="Fade__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade__lava_lib_ui_tools.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GPNumeric.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Numeric/GPNumeric.lvlib"/>
				<Item Name="GPString.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/String/GPString.lvlib"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open Create Replace DWDT Array Dlog File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Open Create Replace DWDT Array Dlog File.vi"/>
				<Item Name="Open Create Replace WDT Array Dlog File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Open Create Replace WDT Array Dlog File.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="POffsetRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/POffsetRect.vi"/>
				<Item Name="PointInRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/PointInRect.vi"/>
				<Item Name="Read DWDT Array Dlog File+.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Read DWDT Array Dlog File+.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Waveform from File (Digital).vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Read Waveform from File (Digital).vi"/>
				<Item Name="Read Waveform from File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Read Waveform from File.vi"/>
				<Item Name="Read Waveforms from File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Read Waveforms from File.vi"/>
				<Item Name="Read WDT Array Dlog File+.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Read WDT Array Dlog File+.vi"/>
				<Item Name="RectAndRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectAndRect.vi"/>
				<Item Name="RectCentroid.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectCentroid.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="SetTransparency(Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency(U8)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(U8)__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency__lava_lib_ui_tools.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write DWDT Dlog File+.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Write DWDT Dlog File+.vi"/>
				<Item Name="Write Waveforms to File.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Write Waveforms to File.vi"/>
				<Item Name="Write WDT Array Dlog File+.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTFileIO.llb/Write WDT Array Dlog File+.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Chamber Control GUI.lvlib" Type="Library" URL="../../Chamber Control GUI/Chamber Control GUI.lvlib"/>
			<Item Name="Control Refnum.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/Control Refnum/Control Refnum.lvlib"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
			<Item Name="Memory Table Plugin.lvlibp" Type="LVLibp" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp">
				<Item Name="Public" Type="Folder">
					<Item Name="Initialize Memory Table Plugin.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/Plugin/Memory Table Plugin/Public/Initialize Memory Table Plugin.vi"/>
				</Item>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Create Register Data File.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/SubVIs/Create Register Data File.vi"/>
				<Item Name="Enqueue Message (Single).vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/menus/Categories/User Library/Library/Message Queue/Enqueue Message (Single).vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Main.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/Plugin/Memory Table Plugin/Main.vi"/>
				<Item Name="Message Queue.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/menus/Categories/User Library/Library/Message Queue/Message Queue.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Read Register INI File.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/SubVIs/Read Register INI File.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/Memory Table Plugin.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="MulticolumnListbox.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/MulticolumnListbox/MulticolumnListbox.lvlib"/>
			<Item Name="UI Reference.lvlibp" Type="LVLibp" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp">
				<Item Name="Controls" Type="Folder">
					<Item Name="UI - All UI Objects Refs Memory - Operation.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/Controls/UI - All UI Objects Refs Memory - Operation.ctl"/>
				</Item>
				<Item Name="SubVIs" Type="Folder">
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Array.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Array.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Boolean.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Boolean.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Cluster.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Cluster.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - ColorBox.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ColorBox.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - ColorRamp.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ColorRamp.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - ComboBox.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ComboBox.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - DAQmxName.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DAQmxName.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - DataValRefNum.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DataValRefNum.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Digital.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Digital.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - DigitalGraph.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - DigitalGraph.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Enum.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Enum.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - GraphChart.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - GraphChart.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - IntensityChart.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - IntensityChart.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - IntensityGraph.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - IntensityGraph.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Knob.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Knob.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - ListBox.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - ListBox.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - LVObjectRefNum.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - LVObjectRefNum.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - MixedCheckbox.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MixedCheckbox.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - MixedSignalGraph.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MixedSignalGraph.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - MulticolumnListbox.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MulticolumnListbox.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - MultiSegmentPipe.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - MultiSegmentPipe.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - NamedNumeric.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - NamedNumeric.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Numeric.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Numeric.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - NumericWithScale.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - NumericWithScale.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - PageSelector.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - PageSelector.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Path.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Path.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Picture.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Picture.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Pixmap.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Pixmap.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - RadioButtonsControl.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - RadioButtonsControl.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - RefNum.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - RefNum.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Ring.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Ring.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - SceneGraphDisplay.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - SceneGraphDisplay.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Scrollbar.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Scrollbar.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Slide.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Slide.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - String.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - String.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - SubPanel.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - SubPanel.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - TabControl.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TabControl.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - Table.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - Table.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - TreeControl.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TreeControl.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - TypedRefNum.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - TypedRefNum.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - VIRefNum.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - VIRefNum.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformChart.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformChart.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformData.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformData.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - WaveformGraph.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - WaveformGraph.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Get Reference - XYGraph.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Get Reference - XYGraph.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - String version.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - String version.vi"/>
					<Item Name="UI - All UI Objects Refs Memory - Typedef version.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/SubVIs/UI - All UI Objects Refs Memory - Typedef version.vi"/>
				</Item>
				<Item Name="Init Buttons.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/Init Buttons.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="UI - All UI Objects Refs Memory - Get Reference.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/UI - All UI Objects Refs Memory - Get Reference.vi"/>
				<Item Name="UI - All UI Objects Refs Memory.vi" Type="VI" URL="../../../../Exe/Luxshare-OET/Libraries/Plugin/UI Reference.lvlibp/SubVIs/UI - All UI Objects Refs Memory.vi"/>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="VI.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/VI Refnum/VI.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Chamber Function" Type="Packed Library">
				<Property Name="Bld_buildCacheID" Type="Str">{28B6B447-272B-4FFE-B23B-6662E0A951DC}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">V1.0.0</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Chamber Function</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Users/AOI/Desktop/Luxshare-OET/Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/NI_AB_PROJECTNAME</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B6541439-E732-428E-958F-8F4719E88934}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Chamber Function.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Users/AOI/Desktop/Luxshare-OET/Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/NI_AB_PROJECTNAME/Chamber Function.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Users/AOI/Desktop/Luxshare-OET/Exe/Luxshare-OET/Libraries/Plugin/Chamber Control/NI_AB_PROJECTNAME</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{EA32E24A-3C9A-4541-9177-41A31680FF63}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Chamber Function.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Support Library</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">Luxshare-OET</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Chamber Function Module</Property>
				<Property Name="TgtF_internalName" Type="Str">Chamber Function</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright (c) 2017-2018 Luxshare-OET Corporation. All rights reserved</Property>
				<Property Name="TgtF_productName" Type="Str">Chamber Function</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F80FBC05-C831-4666-97F2-75D7D03C7B7F}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Chamber Function.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
